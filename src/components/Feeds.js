import React, {useState} from 'react';
// import Video from 'react-native-video';
import { Text, View, StyleSheet, SafeAreaView, ScrollView, TouchableHighlight,  Dimensions, TouchableOpacity, Image, FlatList } from 'react-native';
// import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import SearchIcon from '../assets/svgs/search_icon.svg';
import MoreIcon from '../assets/svgs/dotdotdot.svg';
import CommentIcon from '../assets/svgs/comment_icon.svg';
import LikeIcon from '../assets/svgs/like_icon.svg';
import ShareIcon from '../assets/svgs/share_icon.svg';
import ArchiveIcon from '../assets/svgs/archive_icon.svg';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { NavigationContainer } from '@react-navigation/native';

// Store width in variable
var width = Dimensions.get('window').width; 

var user_image = require('../assets/images/bw-girl.png');

var face1 = require('../assets/images/faces/face1.jpg');
var face2 = require('../assets/images/faces/face2.jpg');
var face3 = require('../assets/images/faces/face3.jpg');
var face4 = require('../assets/images/faces/face4.jpg');


var post1 = require('../assets/images/posts/1.jpg');
var post2 = require('../assets/images/posts/2.jpg');
var post3 = require('../assets/images/posts/3.jpg');
var post4 = require('../assets/images/posts/4.jpg');


const Feeds = ({navigation}) => {
  // const [currentTime, setCurrentTime] = useState(0);
  // const [duration, setDuration] = useState(0);
  // const [isFullScreen, setIsFullScreen] = useState(false);
  // const [isLoading, setIsLoading] = useState(true);
  // const [paused, setPaused] = useState(false);
  // const [playerState, setPlayerState] = useState(PLAYER_STATES.PLAYING);

  return (
    <SafeAreaView>
      {/* <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} /> */}
      <View style={{backgroundColor: '#ffffff'}}>
        <View style={styles.searchContainer}>
          <TouchableOpacity 
            onPress={()=>{}}
            style={styles.searchButton}
            >
                <SearchIcon/>
          </TouchableOpacity>
        </View>
        <View style={styles.tabs}>
          <TouchableHighlight 
              onPress={()=>{}}
              style={{backgroundColor: '#ffffff', paddingLeft: 30, paddingRight: 30, paddingTop: 10, paddingBottom: 10, borderRadius: 20}}
              > 
            <Text style={{color: '#000000',}}>Feed</Text>
          </TouchableHighlight>
          <TouchableHighlight 
              onPress={()=>{}}
              style={{backgroundColor: '#4552F5', paddingLeft: 30, paddingRight: 30, paddingTop: 10, paddingBottom: 10, borderRadius: 20}}
              > 
            <Text style={{color: '#ffffff',}}>Live</Text>
          </TouchableHighlight>
          <TouchableHighlight 
              onPress={()=>navigation.navigate('Stories')}
              style={{backgroundColor: '#ffffff', paddingLeft: 30, paddingRight: 30, paddingTop: 10, paddingBottom: 10, borderRadius: 20}}
              > 
            <Text style={{color: '#000000',}}>Story</Text>
          </TouchableHighlight>
        </View>
      </View>
         <FlatList 
            contentContainerStyle={{ flexGrow: 1, paddingBottom: 100 }}
            // numColumns={2}
                keyExtractor={(item, index) => index.toString()}
                data={postApi}
                renderItem={({item}) => 
         <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.navigate('Stories')}>
           <View style={styles.postCard}>
           <View style={styles.userInfo}>
             <View style={styles.userProfile}>
              <Image source={item.profileIcon} style={{height:50, width: 50, borderRadius:50}} />
              <Text style={{color: '#000000', marginTop: 10, marginLeft: 5}}>{item.username}</Text>
             </View>
            <View style={styles.moreContainer}>
              <Text style={{color: '#000000',  marginTop: 10}}>2 mins ago</Text>
              <View style={{ marginTop: 20, marginLeft: 10}}>
                <MoreIcon/>
              </View>
            </View>
           </View>
           {/* Video goes here  */}
           <View style={styles.videoPlayerContainer}>
           <Image source={user_image} style={{height:200, width: width-40, borderRadius:5, marginTop: 5}} />
            <View style={{flexDirection:'row', justifyContent:'space-between', marginTop: 10}}>
              <View style={{flexDirection:'row'}}>
                <View style={styles.postActionIcon}>
                  <CommentIcon/>
                  <Text style={styles.iconText}>16</Text>
                </View>
                <View style={styles.postActionIcon}>
                  <LikeIcon/>
                  <Text style={styles.iconText}>31</Text>
                </View>
                <View style={styles.postActionIcon}>
                  <View style={styles.icon}>
                    <ShareIcon/>
                  </View>
                  <Text style={styles.iconText}>11</Text>
                </View>
              </View>
              <View>
              <ArchiveIcon/>
              </View>
            </View>
           </View>

           {/* Post Description */}
           <View style={{marginTop: 10}}>
             <Text style={{color: '#000000',}}>{item.description}</Text>
           </View>
         </View>
         </TouchableOpacity>
                }/>
    </SafeAreaView>
  );
}


var postApi = [
  {
      profileIcon:face1,
      username:'Josh',
      post:post1,
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.'
  },
  {
      profileIcon:face2,
      username:'Kehlina',
      post:post2,
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.'
  },
  {
      profileIcon:face3,
      username:'Snow',
      post:post3,
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.'
  },]


const styles = StyleSheet.create({
    backgroundLayout: {
        
    },
    searchButton:{
      backgroundColor: '#EBEFFE',
      height:50,
      width: 50,
      borderRadius: 10,
      padding:10,
      marginLeft: 'auto'
    },
    searchContainer:{
      width: width,
      flexDirection: 'row',
      padding: 10,
      // backgroundColor: 'red'
    },
    tabs: {
      flexDirection: 'row',
      justifyContent: 'space-evenly'
    },
    postCard: {
      backgroundColor: '#ffffff',
      padding: 20
    },
    userInfo: {
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    userProfile:{
      flexDirection: 'row',
    },
    moreContainer:{
      flexDirection: 'row',
    },
    videoPlayerContainer: {},
    postActionIcon: {
      flexDirection:'row',
      paddingRight: 18
    },
    icon:{
      maxHeight: 10
    },
    iconText:{
      fontSize: 16,
      paddingLeft: 4,
      color: Colors.black
    },
    postVideo:{}
  });

export default Feeds;