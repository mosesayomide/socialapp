import React, {useState} from 'react';
// import Video from 'react-native-video';
import { Text, View, StyleSheet, SafeAreaView,  Dimensions, FlatList, TouchableOpacity, Image, ScrollView } from 'react-native';

import BackIcon from '../assets/svgs/back_icon.svg';


// Store width in variable
var width = Dimensions.get('window').width; 

var face1 = require('../assets/images/faces/face1.jpg');
var face2 = require('../assets/images/faces/face2.jpg');
var face3 = require('../assets/images/faces/face3.jpg');
var face4 = require('../assets/images/faces/face4.jpg');
var face5 = require('../assets/images/faces/face5.jpg');
var face6 = require('../assets/images/faces/face6.jpg');
var face7 = require('../assets/images/faces/face3.jpg');
var face8 = require('../assets/images/faces/face2.jpg');
var face9 = require('../assets/images/faces/face4.jpg');
var face10 = require('../assets/images/faces/face6.jpg');

var post1 = require('../assets/images/posts/1.jpg');
var post2 = require('../assets/images/posts/2.jpg');
var post3 = require('../assets/images/posts/3.jpg');
var post4 = require('../assets/images/posts/4.jpg');
var post5 = require('../assets/images/posts/5.jpg');
var post6 = require('../assets/images/posts/6.jpg');
var post7 = require('../assets/images/posts/7.jpg');
var post8 = require('../assets/images/posts/8.jpg');
var post9 = require('../assets/images/posts/9.jpg');
var post10 = require('../assets/images/posts/10.jpg');


const fetchPost = ()=>{
    for(var i=0; i<storyApi.length; i++){
        return <Text>{i}</Text>
    }
}


const Story = ({navigation}) => {
    
    return (
        <SafeAreaView>
      {/* <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} /> */}

      {/* <ScrollView> */}
      <View style={{backgroundColor: '#ffffff'}}>
      <View style={styles.backContainer}>
          <TouchableOpacity activeOpacity={0.6}
            onPress={()=>navigation.navigate('Feeds')}
            style={styles.backButton}
            >
                <BackIcon/>
          </TouchableOpacity>

          <Text style={{color: 'black', marginTop: 10, left: 10, fontSize: 18, fontWeight:'800'}}>Story</Text>
        </View>

        <View style={{marginLeft: 14}}>

            {/* {storyApi.map((data, i) => (
                <View style={styles.storyCard}>
                    <View style={{position: 'absolute'}}>
                        <Image source={data.post} style={{width: width*0.46, borderRadius:50}} />
                    </View>
                    <View >

                    <Image source={data.profileIcon} style={{height:50, width: 50, borderRadius:50}} />
                    <Text>{data.username}</Text>
                    </View>
                </View>
            ))} */}
            
            <FlatList 
            contentContainerStyle={{ flexGrow: 1, paddingBottom: 200 }}
            numColumns={2}
                keyExtractor={(item, index) => index.toString()}
                data={storyApi}
                renderItem={({item}) => 
                <View style={styles.storyCard}>
                    <View style={{position: 'absolute'}}>
                        <Image source={item['post']} style={{width: (width/2)-30, height: (width/2)+10, borderRadius:15}} />
                    </View>
                    <View style={{position: 'absolute', bottom: 10, left: 5, flexDirection: 'row'}} >
                        <Image source={item.profileIcon} style={{height:30, width: 30, borderRadius:50}} />
                    <Text style={{marginTop: 5, left: 5}}>@{item.username}</Text>
                    </View>
                </View>
            }
            />
      </View>
      </View>
      {/* </ScrollView> */}
      </SafeAreaView>);}

var storyApi = [
    {
        profileIcon:face1,
        username:'clara',
        post:post1
    },
    {
        profileIcon:face2,
        username:'clara',
        post:post2
    },
    {
        profileIcon:face3,
        username:'clara',
        post:post3
    },
    {
        profileIcon:face4,
        username:'clara',
        post:post4
    },
    {
        profileIcon:face5,
        username:'clara',
        post:post5
    },
    {
        profileIcon:face6,
        username:'clara',
        post:post6
    },
    {
        profileIcon:face7,
        username:'clara',
        post:post7
    },
    {
        profileIcon:face8,
        username:'clara',
        post:post8
    },
    {
        profileIcon:face9,
        username:'clara',
        post:post9
    },
    {
        profileIcon:face10,
        username:'clara',
        post:post10
    },
]

const styles = StyleSheet.create({
    backgroundLayout: {
        
    },
    backButton:{
      backgroundColor: '#EBEFFE',
      height:40,
      width: 40,
      borderRadius: 10,
      padding:10,
    //   marginLeft: 'auto'
    },
    backContainer:{
      width: width,
      padding: 10,
      flexDirection: 'row'
      // backgroundColor: 'red'
    },
    storyCard:{
        marginTop: 20,
        width: width*0.5,
        height: (width/2)+10,
        position: 'relative',
    }
  });

export default Story;